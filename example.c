/*
    Example.c: A demonstative example
    Copyright 2012 Sulaiman Mustafa <seininn@hush.com>
    License: GPLv3 (Contact me for a possible alternative license)
    Webpage: people.sigh.asia/~sulaiman/software/libraries/clia
*/

#include <stdio.h>
#include <stdlib.h>
#include "clia.h"


int main(int argc, char **argv){
    int e;

    atexit(clia_clear);
    
    clia_add("dud", NULL);
    clia_add("help", "h");
    clia_add("loop", "l");

    if ((e=clia_parse(argc, argv))) {
        printf("`%s' is not a valid option. See --help for usage information.\n", argv[e]);
        return 1;
    }
    
    if (clia_get("help", "index")) {
        printf( "valid options are --help, -h and --loop, -l.\n"
                "%d Help arguments recieved!\n", 
                clia_get("help", "count"));
        return 1;
    }
    else if (clia_get("dud", "index")) {
        puts("found it!");
        return 0;
    }
    
    do puts(argv[argc-1]); while (clia_get("loop", "index"));
    
    
    return 0;

}

