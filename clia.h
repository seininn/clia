/*    
    Clia: Command line option processing in a convenient package (.c/.h)

    Copyright (C) 2012  Sulaiman A. Mustafa
    
    [ Alternative licensing is available ]
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    
    Contact me if an alternative license is desired. Depending on the type of 
    intended use, an alternative license might be granted free of charge, in 
    exchange for compensation, or it might not be granted at all.
    
    Contact: http://people.sigh.asia/~sulaiman/about/contact/
    
*/

#ifndef CLIA_H
#define CLIA_H

int clia_add(char *long_option, char *short_option);
/*
    Adds options for the parser to look for.
    
    Parameters:
        
        long_option     the long option name
        short_option    the short one. can be NULL

    Returns:
        
        0 on success and 1 if out of memory.
*/


int clia_parse(int argc, char **argv);
/*
    Parses the comandline options.
    
    Parameters:
        
        argc            --self explanatory--
        argv            --self explanatory--
    
    Returns:
        
        0 on sucess or the index of the first undefined option found.

*/

int clia_get(char *long_option, char *resource); 
/*
    Gets information about the argument once parsed.
    
    Parameters:
        
        long_option     the registered long option name
        resource        what type information, which can be ether "index" or 
                        "count"
    Resources:
        
        The resources returned can be ether the index of the option itself, or 
        the number of normal arguments that come after it and before the next 
        option, i.e the option's arguments.
        
        Requesting the option index is done by passing "index", and requesting
        the option argument count is done by passing "count".
    
    Returns:
        
        the index of the option or 0 if not found.
*/


void clia_clear();
/*
    clear all data generated and release allocated memory. sutable for atexit(3)
*/


#endif
